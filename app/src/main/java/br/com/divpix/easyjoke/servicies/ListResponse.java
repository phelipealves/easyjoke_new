package br.com.divpix.easyjoke.servicies;

import java.util.List;

/**
 * Defines the API response for a list of the DataObject and with pagination
 *
 * @param <DataObject> object type for request
 * @see Response
 * @see Pagination
 */
public class ListResponse<DataObject> extends Response<List<DataObject>> {

    /**
     * @see Pagination
     */
    private Pagination pagination;

    /**
     * Get pagination from the response
     *
     * @return pagination from the response
     */
    public Pagination getPagination() {
        return pagination;
    }

    /**
     * Set {@link Pagination}
     *
     * @param pagination {@link Pagination} from the response
     */
    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

}