package br.com.divpix.easyjoke.net;

public class NetworkStateChanged {

    private boolean mIsInternetConnected;

    private NetworkStateChanged(boolean isInternetConnected) {
        this.mIsInternetConnected = isInternetConnected;
    }

    public static NetworkStateChanged setConnectivity(boolean connectivity){
        return new NetworkStateChanged(connectivity);
    }

    public boolean isInternetConnected() {
        return this.mIsInternetConnected;
    }
}
