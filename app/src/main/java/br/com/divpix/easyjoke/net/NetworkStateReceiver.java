package br.com.divpix.easyjoke.net;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import de.greenrobot.event.EventBus;

public class NetworkStateReceiver extends BroadcastReceiver {
    private static String LOG_TAG = NetworkStateReceiver.class.getSimpleName();

    EventBus eventBus = EventBus.getDefault();

    @Override
    public void onReceive(Context context, Intent intent) {
        if (NetworkState.isNetworkAvailable(context)){
            eventBus.post(NetworkStateChanged.setConnectivity(true));
        } else {
            eventBus.post(NetworkStateChanged.setConnectivity(false));
        }
    }

}
