package br.com.divpix.easyjoke.adapters;

import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.divpix.easyjoke.R;
import br.com.divpix.easyjoke.models.Joke;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 *
 */
public class JokeAdapter extends  RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<Joke> mValues;

    public JokeAdapter() {
        this.mValues = new ArrayList<Joke>();
    }

    public JokeAdapter(List<Joke> mValues) {
        this.mValues = mValues;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_joke, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemViewHolder){
            ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
            itemViewHolder.jokeTitle.setText(mValues.get(position).getTitle());
            itemViewHolder.jokeContent.setText(Html.fromHtml(mValues.get(position).getContent()));
        }
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public void appendItems(List<Joke> items) {
        int count = getItemCount();
        mValues.addAll(items);
        notifyItemRangeInserted(count, items.size());
    }

    public Joke getItem(int position) {
        return mValues.get(position);
    }

    //    public JokeAdapter(RecyclerView recyclerView, List<Joke> dataSet, int visibleThreshold, OnLoadMoreListener onLoadMoreListener) {
//        super(recyclerView, dataSet, visibleThreshold, onLoadMoreListener);
//    }
//
//    @Override
//    public RecyclerView.ItemViewHolder onCreateBasicItemViewHolder(ViewGroup parent, int viewType) {
//        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_joke, parent, false);
//        return new ItemViewHolder(view);
//    }
//
//    @Override
//    public void onBindBasicItemView(RecyclerView.ItemViewHolder genericHolder, int position) {
//        ItemViewHolder holder = (ItemViewHolder) genericHolder;
//        holder.jokeTitle.setText(getItem(position).getTitle());
//        holder.jokeContent.setText(Html.fromHtml(getItem(position).getContent()));
//    }

    static class ItemViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.joke_title)
        TextView jokeTitle;
        @Bind(R.id.joke_content)
        TextView jokeContent;

        public ItemViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
