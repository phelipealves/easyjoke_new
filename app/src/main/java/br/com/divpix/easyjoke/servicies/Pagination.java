package br.com.divpix.easyjoke.servicies;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

/**
 * Paging for a data list
 *
 * @see ListResponse
 */
public class Pagination {

    @SerializedName("page_count")
    private int pageCount;
    @SerializedName("current_page")
    private int currentPage;
    @SerializedName("has_next_page")
    private boolean hasNextPage;
    @SerializedName("has_prev_page")
    private boolean hasPreviousPage;
    @SerializedName("count")
    private int count;
    @SerializedName("limit")
    private int limit;

    /**
     * Get total of page
     *
     * @return integer containing o number of pages
     */
    public int getPageCount() {
        return pageCount;
    }

    /**
     * Set total of page
     *
     * @param pageCount number of pages
     */
    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    /**
     * Get current page
     *
     * @return integer containing the number of the current page
     */
    public int getCurrentPage() {
        return currentPage;
    }

    /**
     * Set current page
     *
     * @param currentPage number of current page
     */
    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    /**
     * Verify if has a next page
     *
     * @return true if has a next page otherwise false
     */
    public boolean hasNextPage() {
        return hasNextPage;
    }

    /**
     * Set if has a next page
     *
     * @param hasNextPage boolean if has a next page
     */
    public void setHasNextPage(boolean hasNextPage) {
        this.hasNextPage = hasNextPage;
    }

    /**
     * Get next page number
     * @return nextPage integer for the next page
     */
    public int getNextPage() {
        if(this.hasNextPage()){
            return this.getCurrentPage() + 1;
        }
        return 0;
    }

    /**
     * Verify if has a previous page
     *
     * @return true if has a previous page otherwise false
     */
    public boolean hasPreviousPage() {
        return hasPreviousPage;
    }

    /**
     * Set if has a previous page
     *
     * @param hasPreviousPage boolean if has a previous page
     */
    public void setHasPreviousPage(boolean hasPreviousPage) {
        this.hasPreviousPage = hasPreviousPage;
    }

    /**
     * Get count of the items
     *
     * @return integer containing the number of the items
     */
    public int getCount() {
        return count;
    }

    /**
     * Set count of returned items
     *
     * @param count number of items
     */
    public void setCount(int count) {
        this.count = count;
    }

    /**
     * Get request limit
     *
     * @return integer containing the limit number
     */
    public int getLimit() {
        return limit;
    }

    /**
     * Set limit number
     *
     * @param limit number
     */
    public void setLimit(int limit) {
        this.limit = limit;
    }


    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
