package br.com.divpix.easyjoke.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rockerhieu.rvadapter.endless.EndlessRecyclerViewAdapter;

import java.util.List;

import br.com.divpix.easyjoke.R;
import br.com.divpix.easyjoke.activities.JokeActivity;
import br.com.divpix.easyjoke.activities.MainActivity;
import br.com.divpix.easyjoke.adapters.JokeAdapter;
import br.com.divpix.easyjoke.models.Category;
import br.com.divpix.easyjoke.models.CategoryRepository;
import br.com.divpix.easyjoke.models.Joke;
import br.com.divpix.easyjoke.models.JokeRepository;
import br.com.divpix.easyjoke.net.NetworkStateChanged;
import br.com.divpix.easyjoke.presenters.CategoryPresenter;
import br.com.divpix.easyjoke.servicies.JokeService;
import br.com.divpix.easyjoke.servicies.ListResponse;
import br.com.divpix.easyjoke.servicies.Pagination;
import br.com.divpix.easyjoke.view.RecyclerUtils.RecyclerItemClickListener;
import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;

import static br.com.divpix.easyjoke.view.RecyclerUtils.RecyclerItemClickListener.OnItemClickListener;

public class CategoryFragment extends Fragment implements EndlessRecyclerViewAdapter.RequestToLoadMoreListener {
    private static final String LOG_TAG = CategoryFragment.class.getSimpleName();

    private static final String ARG_CATEGORY_ID = "category_id";
    @Bind(R.id.list)
    RecyclerView mRecyclerView;
    private Context mContext;
    private RecyclerView.LayoutManager mLayoutManager;
    private JokeAdapter mAdapter;
    private EndlessRecyclerViewAdapter mEndlessRecyclerViewAdapter;
    private Pagination mPagination;
    private Integer mCategoryId;
    private CategoryPresenter mCategoryPresenter;
    private JokeService mJokeService;
    private EventBus eventBus = EventBus.getDefault();
    private boolean mError;
    private Category mCategory;

    public CategoryFragment() {

    }

    /**
     * @param categoryId
     * @return
     */
    public static CategoryFragment newInstance(int categoryId) {
        CategoryFragment fragment = new CategoryFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_CATEGORY_ID, categoryId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mCategoryId = getArguments().getInt(ARG_CATEGORY_ID);
        }

        // Save this context
        mContext = this.getActivity();

        mCategory = CategoryRepository.getForId(mContext, mCategoryId);

        // Service
        mJokeService = new JokeService();
        mCategoryPresenter = new CategoryPresenter(this, mJokeService);

        // Event Bus
        eventBus.register(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_category, container, false);
        ButterKnife.bind(this, view);

        mLayoutManager = new LinearLayoutManager(mContext);
        mRecyclerView.setLayoutManager(mLayoutManager);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mAdapter = new JokeAdapter();
        mEndlessRecyclerViewAdapter = new EndlessRecyclerViewAdapter(getActivity(), mAdapter, this);
        mRecyclerView.setAdapter(mEndlessRecyclerViewAdapter);
        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(mContext, new OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Joke joke = mAdapter.getItem(position);
                        if (joke != null) {
                            openJoke(joke.getId());
                        }
                    }
                }));

        mCategoryPresenter.loadJokes();

        if(mCategory != null){
            ((MainActivity)getActivity()).getSupportActionBar().setTitle(mCategory.getName());
        }
    }

    @Override
    public void onLoadMoreRequested() {
        if (mPagination == null || mPagination.hasNextPage()) {
            mCategoryPresenter.loadJokes();
        }
    }

    public void displayJokes(ListResponse<Joke> jokeListResponse) {
        mError = false;
        mPagination = jokeListResponse.getPagination();
        List<Joke> jokes = jokeListResponse.getData();

        // Save in local database
        saveInDatabase(jokes);

        // Append jokes in adapter list
        mAdapter.appendItems(jokes);

        // Stop or not load more request
        if (mPagination != null) {
            if (mPagination.hasNextPage()) {
                mEndlessRecyclerViewAdapter.onDataReady(true);
            } else {
                mEndlessRecyclerViewAdapter.onDataReady(false);
            }
        }
    }

    private void saveInDatabase(List<Joke> jokes) {
        for (Joke joke : jokes) {
            Category category = CategoryRepository.getForId(mContext, mCategoryId);
            if (category != null) {
                joke.setCategory(category);
            }
            JokeRepository.insertOrUpdate(mContext, joke);
        }
    }

    public void displayError() {
        mError = true;
        mEndlessRecyclerViewAdapter.onDataReady(false);
    }

    /**
     * Get next page
     *
     * @return
     */
    public int getNextPage() {
        if (mPagination != null && mPagination.hasNextPage()) {
            return mPagination.getNextPage();
        } else {
            return 0;
        }
    }

    /**
     * Get category id
     *
     * @return
     */
    public int getCategoryId() {
        return mCategoryId;
    }

    public void openJoke(long id) {
        Intent intent = new Intent(getActivity(), JokeActivity.class);
        Bundle bundle = new Bundle();
        bundle.putLong(JokeActivity.JOKE_ID, id);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    public void onEvent(NetworkStateChanged stateChanged) {
        Log.i(LOG_TAG, "" + stateChanged.isInternetConnected());
        if (stateChanged.isInternetConnected()) {
            if (mPagination == null && mError == true) {
                onLoadMoreRequested();
            }
            if (mPagination != null && mPagination.hasNextPage()) {
                mEndlessRecyclerViewAdapter.onDataReady(true);
            }
        }
    }

    @Override
    public void onDestroy() {
        eventBus.unregister(this);
        super.onDestroy();
    }
}
