package br.com.divpix.easyjoke.activities;

import android.content.Intent;
import android.content.res.Resources;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import br.com.divpix.easyjoke.R;
import br.com.divpix.easyjoke.models.Category;
import br.com.divpix.easyjoke.models.Joke;
import br.com.divpix.easyjoke.models.JokeRepository;
import butterknife.Bind;
import butterknife.ButterKnife;

public class JokeActivity extends AppCompatActivity {
    private static final String LOG_TAG = JokeActivity.class.getSimpleName();
    public static final String JOKE_ID = "joke_id";

    // View
    @Bind(R.id.floating_share_action)
    FloatingActionButton mFloatingShareAction;
    @Bind(R.id.joke_content)
    TextView mJokeContent;
    @Bind(R.id.toolbar)
    Toolbar mToolbar;

    // Variables
    private long mJokeId;
    private Joke mJoke;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_joke);
        ButterKnife.bind(this);

        // Action Bar
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mJokeId = getIntent().getExtras().getLong(JOKE_ID);
        mJoke = JokeRepository.getForId(this, mJokeId);
        if (mJoke != null){
            // Display title and subtitle
            getSupportActionBar().setTitle(mJoke.getTitle());
            Category category = mJoke.getCategory();;
            if (category != null){
                getSupportActionBar().setSubtitle(category.getName());
            }
            // Display joke content
            mJokeContent.setText(Html.fromHtml(mJoke.getContent()));
            // Share action
            mFloatingShareAction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(Intent.createChooser(getShareIntent(),
                            getString(R.string.share)));
                }
            });
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private Intent getShareIntent() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_TEXT, formattedJokeToShare());
        intent.setType("text/plain");
        return intent;
    }

    protected String formattedJokeToShare(){
        StringBuilder result = new StringBuilder();
        result.append(mJoke.getTitle()).append("\n\n");
        result.append(Html.fromHtml(mJoke.getContent()));
        Resources res = getResources();
        result.append(String.format(res.getString(R.string.by_app), res.getString(R.string.app_name)));
        return result.toString();
    }
}
