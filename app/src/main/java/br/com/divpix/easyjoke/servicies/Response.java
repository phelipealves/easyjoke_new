package br.com.divpix.easyjoke.servicies;

/**
 * Defines the API response of a single object
 *
 * @param <DataObject> object type for request
 */
public class Response<DataObject> {

    private boolean success;
    private DataObject data;

    /**
     * Request status
     *
     * @return true if the request is successful otherwise false
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * Set request status
     *
     * @param success boolean for define status
     */
    public void setSuccess(boolean success) {
        this.success = success;
    }

    /**
     * Get response object
     *
     * @return object or null if not exists data
     */
    public DataObject getData() {
        return data;
    }

    /**
     * Set response object
     *
     * @param data object of response
     */
    public void setData(DataObject data) {
        this.data = data;
    }
}
