package br.com.divpix.easyjoke.models;

import android.content.Context;

import java.util.List;

import br.com.divpix.easyjoke.EasyJokeApplication;

public class CategoryRepository {

    public static void insertOrUpdate(Context context, Category category) {
        getDao(context).insertOrReplace(category);
    }

    public static void clearAll(Context context) {
        getDao(context).deleteAll();
    }

    public static void deleteWithId(Context context, long id) {
        getDao(context).delete(getForId(context, id));
    }

    public static List<Category> getAll(Context context) {
        return getDao(context).loadAll();
    }

    public static Category getForId(Context context, long id) {
        return getDao(context).load(id);
    }

    private static CategoryDao getDao(Context c) {
        return ((EasyJokeApplication) c.getApplicationContext()).getDaoSession().getCategoryDao();
    }
}
