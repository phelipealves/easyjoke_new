package br.com.divpix.easyjoke.presenters;

import android.util.Log;

import br.com.divpix.easyjoke.fragments.CategoryFragment;
import br.com.divpix.easyjoke.models.Joke;
import br.com.divpix.easyjoke.servicies.JokeService;
import br.com.divpix.easyjoke.servicies.ListResponse;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by phelipe on 09/10/15.
 */
public class CategoryPresenter {

    CategoryFragment mView;
    JokeService mJoke;

    public CategoryPresenter(CategoryFragment view, JokeService joke) {
        this.mView = view;
        this.mJoke = joke;
    }

    public void loadJokes(){
        mJoke.getApi()
                .jokesFromCategory(mView.getCategoryId(), mView.getNextPage())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ListResponse<Joke>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(CategoryPresenter.class.getSimpleName(), e.getMessage());
                        mView.displayError();
                    }

                    @Override
                    public void onNext(ListResponse<Joke> jokeListResponse) {
                        mView.displayJokes(jokeListResponse);
                    }
                });
    }
}
