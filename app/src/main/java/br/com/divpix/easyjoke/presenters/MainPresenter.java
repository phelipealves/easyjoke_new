package br.com.divpix.easyjoke.presenters;

import android.util.Log;

import br.com.divpix.easyjoke.activities.MainActivity;
import br.com.divpix.easyjoke.models.Category;
import br.com.divpix.easyjoke.servicies.JokeService;
import br.com.divpix.easyjoke.servicies.ListResponse;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MainPresenter {

    MainActivity mView;
    JokeService mJokeService;

    public MainPresenter(MainActivity view, JokeService jokeService) {
        this.mView = view;
        this.mJokeService = jokeService;
    }

    public void loadCategories(){
        mJokeService.getApi()
                .getCategories()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ListResponse<Category>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(MainPresenter.class.getSimpleName(), e.getMessage());
                    }

                    @Override
                    public void onNext(ListResponse<Category> categoryListResponse) {
                        mView.displayCategories(categoryListResponse.getData());
                    }
                });
    }
}
