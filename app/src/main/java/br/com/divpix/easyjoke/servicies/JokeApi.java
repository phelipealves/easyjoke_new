package br.com.divpix.easyjoke.servicies;

import br.com.divpix.easyjoke.models.Category;
import br.com.divpix.easyjoke.models.Joke;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;
import rx.Observable;

/**
 * 
 * @author Phelipe Alves de Souza
 */
public  interface JokeApi {
    Integer LIMIT = 10;
    String SERVER_URL = "http://easyjoke.tk/api";
    
    @GET("/jokes/{id}")
    void joke(@Path("id") long id, Callback<Response<Joke>> response);

    @GET("/jokes/{id}")
    Response<Joke> joke(@Path("id") long id);
    
//    @GET("/jokes")
//    ListResponse<Joke> jokes();
//
//    @GET("/jokes")
//    void jokesFromCategory(@Query("category") int category_id, Callback<ListResponse<Joke>> response);
//
//    @GET("/jokes")
//    ListResponse<Joke> jokesFromCategory(@Query("category") int category_id);

    @GET("/jokes")
    Observable<ListResponse<Joke>> jokesFromCategory(@Query("category") int category_id, @Query("page") int page);

//    @GET("/jokes/")
//    void jokes(Callback<ListResponse<Joke>> response);
//
//    @GET("/categories")
//    void categories(Callback<ListResponse<Category>> response);

//
//    @GET("/categories")
//    ListResponse<Category> categories();

    @GET("/categories")
    Observable<ListResponse<Category>> getCategories();
    
}
