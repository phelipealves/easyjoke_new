package br.com.divpix.easyjoke.activities;

import android.content.Context;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.HeaderViewListAdapter;
import android.widget.ListView;

import java.util.List;

import br.com.divpix.easyjoke.R;
import br.com.divpix.easyjoke.fragments.CategoryFragment;
import br.com.divpix.easyjoke.models.Category;
import br.com.divpix.easyjoke.models.CategoryRepository;
import br.com.divpix.easyjoke.net.NetworkStateChanged;
import br.com.divpix.easyjoke.presenters.MainPresenter;
import br.com.divpix.easyjoke.servicies.JokeService;
import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private static final String LOG_TAG = MainActivity.class.getSimpleName();
    private static final String CATEGORY_ID = "CATEGORY_ID";
    private static final String CATEGORY_TITLE = "CATEGORY_TITLE";

    // View
    @Bind(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    @Bind(R.id.navigation_view)
    NavigationView mNavigationView;
    @Bind(R.id.toolbar)
    Toolbar mToolbar;

    // Variables
    private Context mContext;
    private EventBus eventBus = EventBus.getDefault();
    private int mCategoryId;
    private CharSequence mCategoryTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        if (savedInstanceState != null){
            mCategoryId = savedInstanceState.getInt(CATEGORY_ID);
            mCategoryTitle = savedInstanceState.getCharSequence(CATEGORY_TITLE);
        }

        // Action Bar
        setSupportActionBar(mToolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Save this context
        mContext = this;

        // Naviagation Layout
        mNavigationView.setNavigationItemSelectedListener(this);

        // Load from local categories
        List<Category> localCategories = CategoryRepository.getAll(mContext);
        loadNavigationItems(localCategories);
        Log.d(LOG_TAG, "Total saved categories = " + localCategories.size());

        // Load from server categories
        JokeService jokeService = new JokeService();
        MainPresenter mainPresenter = new MainPresenter(this, jokeService);
        mainPresenter.loadCategories();

        // EventBus
        eventBus.register(this);
    }

    /**
     * Display categories in NavegationView
     *
     * @param categories
     */
    public void displayCategories(List<Category> categories) {
        Log.d(LOG_TAG, "Total of categories loaded from server = " + categories.size());
        mNavigationView.getMenu().clear();
        loadNavigationItems(categories);
    }

    /**
     * Add category item menu in NavegationView and save in database
     *
     * @param categories
     */
    private void loadNavigationItems(List<Category> categories) {
        for (Category category : categories) {
            mNavigationView.getMenu().add(0, category.getId().intValue(), category.getId().intValue(), category.getName());
            CategoryRepository.insertOrUpdate(mContext, category);
        }
        updateNavegation();
    }

    /**
     * Update the NavigationView when there is a change
     */
    private void updateNavegation() {
        for (int i = 0, count = mNavigationView.getChildCount(); i < count; i++) {
            final View child = mNavigationView.getChildAt(i);
            if (child != null && child instanceof ListView) {
                final ListView menuView = (ListView) child;
                final HeaderViewListAdapter adapter = (HeaderViewListAdapter) menuView.getAdapter();
                final BaseAdapter wrapped = (BaseAdapter) adapter.getWrappedAdapter();
                wrapped.notifyDataSetChanged();
            }
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        mCategoryId = menuItem.getItemId();
        mCategoryTitle = menuItem.getTitle();

        menuItem.setChecked(true);
        getSupportActionBar().setTitle(menuItem.getTitle());

        mDrawerLayout.closeDrawers();
        Fragment fragment = null;
        switch (menuItem.getItemId()) {
            default:
                fragment = CategoryFragment.newInstance(menuItem.getItemId());
                break;
        }
        openFragment(fragment);

        return true;
    }

    /**
     * @param categoryId
     * @return
     */
    private Fragment getFragment(int categoryId) {
        return null;
    }

    /**
     * Open fragment in content view
     *
     * @param fragment
     */
    private void openFragment(Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.content_fragment, fragment);
        ft.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
            case R.id.action_settings:
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (isNavDrawerOpen()) {
            closeNavDrawer();
        } else {
            super.onBackPressed();
        }
    }

    protected boolean isNavDrawerOpen() {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(GravityCompat.START);
    }

    protected void closeNavDrawer() {
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        }
    }

    public void onEvent(NetworkStateChanged stateChanged) {
        if (stateChanged.isInternetConnected()) {
            Log.i(LOG_TAG, "Item selected: " + mCategoryTitle);
        } else {
            Snackbar.make(findViewById(R.id.coordinator), "Sem conexão", Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onDestroy() {
        eventBus.unregister(this);
        super.onDestroy();
    }
}
