package br.com.divpix.easyjoke.net;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkState {
    /**
     *
     * @param context
     * @return
     */
    public static boolean isNetworkAvailable(Context context) {
        boolean isMobile = false, isWifi = false;

        NetworkInfo network = getConnectivityManagerInstance(
                context).getActiveNetworkInfo();
        if (network != null){
            if (network.getType() == ConnectivityManager.TYPE_WIFI) {
                if (network.isConnected() && network.isAvailable())
                    isWifi = true;
            }
            if (network.getType() == ConnectivityManager.TYPE_MOBILE) {
                if (network.isConnected() && network.isAvailable())
                    isMobile = true;
            }
        }

        return isMobile || isWifi;
    }

    /**
     *
     * @param context
     * @return
     */
    private static ConnectivityManager getConnectivityManagerInstance(
            Context context) {
        return (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
    }
}
