package br.com.divpix.easyjoke;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;

import br.com.divpix.easyjoke.models.DaoMaster;
import br.com.divpix.easyjoke.models.DaoSession;

public class EasyJokeApplication extends Application {

    private DaoSession daoSession;

    @Override
    public void onCreate() {
        super.onCreate();
        setupDatabase();
    }

    private void setupDatabase(){
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "easyjoke-db", null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        daoSession = daoMaster.newSession();
    }

    public DaoSession getDaoSession() {
        return daoSession;
    }
}
