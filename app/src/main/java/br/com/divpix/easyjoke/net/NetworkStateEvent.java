package br.com.divpix.easyjoke.net;

public interface NetworkStateEvent {

    public void onEventMainThread(NetworkStateChanged event);
}
