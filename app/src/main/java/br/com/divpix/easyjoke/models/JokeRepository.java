package br.com.divpix.easyjoke.models;

import android.content.Context;

import java.util.List;

import br.com.divpix.easyjoke.EasyJokeApplication;

public class JokeRepository {

    public static void insertOrUpdate(Context context, Joke joke) {
        getDao(context).insertOrReplace(joke);
    }

    public static void clearAll(Context context) {
        getDao(context).deleteAll();
    }

    public static void deleteWithId(Context context, long id) {
        getDao(context).delete(getForId(context, id));
    }

    public static List<Joke> getAll(Context context) {
        return getDao(context).loadAll();
    }

    public static Joke getForId(Context context, long id) {
        return getDao(context).load(id);
    }

    private static JokeDao getDao(Context c) {
        return ((EasyJokeApplication) c.getApplicationContext()).getDaoSession().getJokeDao();
    }
}
