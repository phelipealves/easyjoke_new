package br.com.divpix.easyjoke.servicies;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;

/**
 *
 */
public class JokeService {

    JokeApi mJokeApi;

    private static final RequestInterceptor acceptApplicationJson = new RequestInterceptor() {
        @Override
        public void intercept(RequestFacade request) {
            request.addHeader("Accept", "application/json");
            request.addQueryParam("limit", JokeApi.LIMIT.toString());
        }
    };

    public JokeService() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.NONE)
                .setEndpoint(JokeApi.SERVER_URL)
                .setRequestInterceptor(acceptApplicationJson)
                .build();
        mJokeApi = restAdapter.create(JokeApi.class);
    }

    public JokeApi getApi(){
        return mJokeApi;
    }
}
